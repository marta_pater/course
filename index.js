const express = require('express');
const app = express();

const fs = require('fs');
const jsonParser = express.json();
const morgan = require("morgan");
const filePath = 'files.json';

app.use(morgan('tiny'));

app.post('/api/files', jsonParser, (req, res) => {
      
    if(!req.body) return res.status(400).send("Please specify 'content' parameter");
      
    const fileName = req.body.filename;
    const fileContent = req.body.content;
    const extension = fileName.split('.')[1];
    let file = {message: 'Success', filename: fileName, content: fileContent, extension: extension, uploadedDate: new Date()};
      
    let data = fs.readFileSync(filePath, 'utf8');
    let files = JSON.parse(data);

    files.push(file);
    data = JSON.stringify(files);
    fs.writeFileSync('files.json', data);
    res.send('File created successfully');
    res.status(500).send('Server error');
});

app.get('/api/files',(req, res) => {
    const content = fs.readFileSync(filePath,'utf8');
    const files = JSON.parse(content);
    //res.send(files);
    let filesMas = [];
    for(let i = 0; i < files.length; i++) {
        filesMas.push(files[i].filename);
    }
    let file = {message: 'Success', files: filesMas};
    res.send(file);
    res.status(400).send('Client error');
    res.status(500).send('Server error');
});

app.get('/api/files/:filename', (req, res) => {
    const filename = req.params.filename; 
    const content = fs.readFileSync(filePath, 'utf8');
    const files = JSON.parse(content);
    let file = null;

    for(let i = 0; i < files.length; i++){
        if(files[i].filename === filename){
            file = files[i];
        }
    }

    if(file){
        res.send(file);
    }
    else{
        res.status(400).send(`No file with '${filename}' filename found`);
        res.status(500).send('Server error');
    }
});

app.listen(8080);